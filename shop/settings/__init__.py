from .settings import *

CURRENT_INSTANCE = env('CURRENT_INSTANCE', default='dev')

if CURRENT_INSTANCE == 'prod':
    from .prod import *
else:
    from .dev import *