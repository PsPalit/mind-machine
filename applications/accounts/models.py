import datetime

from django.db import models
from django.contrib.auth.models import AbstractUser as DjangoAbstractUser

from applications.items.models import Item


class User(DjangoAbstractUser):
    dttm_created = models.DateTimeField(default=datetime.datetime.now)
    dttm_deleted = models.DateTimeField()

    SEX_FEMALE = 'F'
    SEX_MALE = 'M'
    SEX_CHOICES = (
        (SEX_FEMALE, 'Female',),
        (SEX_MALE, 'Male',),
    )

    sex = models.CharField(max_length=1,  choices=SEX_CHOICES)
    bought_items = models.ManyToManyField(Item)